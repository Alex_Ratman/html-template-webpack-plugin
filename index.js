const fs = require('fs');
const path = require('path');
const { promisify } = require('util');

const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

class HtmlTemplateWebpackPlugin {
  constructor({ entry, tags, output, resources } = {}) {
    const tagScheme = {
      css: '<link rel="stylesheet" type="text/css" href="@{css}"/>',
      js: '<script type="text/javascript" src="@{js}"></script>'
    };

    this.path = {};
    this.tags = {...tagScheme};

    if (this.getType(tags, 'Object')) {
      this.tags = {...this.tags, ...tags};
    }
    if (this.getType(entry, 'Object') || this.getType(entry, 'String')) {
      this.path.entry = entry;
    }
    if (this.getType(output, 'String')) {
      this.path.output = output;
    }
    if (this.getType(resources, 'Object') || this.getType(resources, 'String')) {
      this.path.resources = resources;
    }
  }

  createTag(type, path) {
    const { resources } = this.path;
    const file = resources && resources[type] ? `${resources[type]}/${path}` : path;
    const tag = this.tags[type];

    if (!tag) throw new Error('File extension has not been recognized - no tag scheme for file');

    return `${tag.replace(`@{${type}}`, file)}`;
  }

  getChunkFilesMap(entrypoints = []) {
    const map = new Map();

    entrypoints.forEach(({ chunks }, name) => {
      const types = chunks
        .reduce((acc, { files }) => [...acc, ...files], [])
        .reduce((acc, file) => {
          const extension = file.split('.').reverse()[0];
          if (!acc.get(extension)) { acc.set(extension, new Set()) }
          acc.get(extension).add(file);
          return acc;
        }, new Map());

      map.set(name, types);
    });

    return map;
  }

  async getHtml(tags, fallbackPath) {
    const {
      entry,
      output = fallbackPath
    } = this.path;

    if (!entry) {
      throw new Error('No Html template specified');
    }

    if (!fs.existsSync(output)) fs.mkdirSync(output, { recursive: true });

    for (let [file, types] of tags) {
      let html = await readFile(path.resolve(process.cwd(), entry[file] || entry), 'utf8');

      types.forEach((codes, type) => {
        codes.forEach(code => {
          html = html.replace(`@{${type}}`, code);
        });
      });

      await writeFile(`${output}/${file}.html`, html, 'utf8');
    }
  }

  getHtmlTagsMap(files) {
    files.forEach(types => {
      types.forEach((paths, type) => {
        let content = '';
        paths.forEach(path => {
          const tag = this.createTag(type, path);
          content = content.length ? `${content}\n${tag}` : tag;
        });
        paths.clear();
        paths.add(content);
      });
    });

    return files;
  }

  getType(element, type) {
    const test = Object.prototype.toString.call(element).slice(8, -1);
    const result = test === type;

    if (!type) {
      return test;
    }

    return result ? element : false;
  }

  apply(compiler) {
    compiler.hooks.emit.tapAsync('Html Template Plugin', async ({ entrypoints }, callback) => {
      try {
        const files = this.getChunkFilesMap(entrypoints);
        const tags = this.getHtmlTagsMap(files);

        await this.getHtml(tags, compiler.outputPath);
      } catch (e) {
        console.error(e);
      }

      callback()
    });
  }
}

module.exports = HtmlTemplateWebpackPlugin;