# HTML Template Webpack Plugin #

Webpack plugin for React SSR HTML template purposes

### Options ###

* **entry** *{Object|String} [required]* - entry template path (basic HTML without scripts/css) - can be different for each page
* **tags** *{Object}* - scheme(s) for injected tags
* **output** *{String}* - path for HTML template output (default Webpack outputhPath as fallback)
* **resources** *{Object|String}* - path parts for different types of files (i.e. '/js/', '/public/css/')

### Example ###

**
```javascript
import HtmlTemplateWebpackPlugin from 'html-template-webpack-plugin';

{
...
plugins: [
  new HtmlTemplateWebpackPlugin({
    entry: {
      about: './src/templates/about.html',
      index: './src/templates/base.html'
    },
    output: './dist',
	resources: {
	  js: 'public/js',
	  css: 'public/css'
	}
  })
],
...
}
```